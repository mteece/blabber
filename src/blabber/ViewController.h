//
//  ViewController.h
//  blabber
//
//  Created by Matthew Teece on 5/28/15.
//  Copyright (c) 2015 Matthew Teece. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHTextField.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *txtChat;
@property (weak, nonatomic) IBOutlet MHTextField *txtMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;

- (void)btnSendTouchUpInside:(id)sender;

@end

