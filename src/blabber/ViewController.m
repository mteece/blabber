//
//  ViewController.m
//  blabber
//
//  Created by Matthew Teece on 5/28/15.
//  Copyright (c) 2015 Matthew Teece. All rights reserved.
//

#import "ViewController.h"
#import <OpenTok/OpenTok.h>
#import "MHTextField.h"

@interface ViewController ()
<UITextViewDelegate, UITextFieldDelegate, OTSessionDelegate>

@end

@implementation ViewController
{
    OTSession *_session;
    OTConnection *_connection;
}

static NSString* const kApiKey = @"45247572";
static NSString* const kSessionId = @"2_MX40NTI0NzU3Mn5-MTQzMjkwODkyMTg5MX5aaGY0VTlDY3VhQnpzSUVkeDAyTzI4Y1V-fg";
static NSString* const kToken = @"T1==cGFydG5lcl9pZD00NTI0NzU3MiZzaWc9ZmY5MTk5NjliOTIzZWIzOGQ3MjE5YWEwMzNkZTg5ZTcyM2M1OGVlMzpyb2xlPXN1YnNjcmliZXImc2Vzc2lvbl9pZD0yX01YNDBOVEkwTnpVM01uNS1NVFF6TWprd09Ea3lNVGc1TVg1YWFHWTBWVGxEWTNWaFFucHpTVVZrZURBeVR6STRZMVYtZmcmY3JlYXRlX3RpbWU9MTQzMjkxMjU1MCZub25jZT0wLjc0ODQzODAxNzA4MzY2NSZleHBpcmVfdGltZT0xNDMyOTMzOTc4JmNvbm5lY3Rpb25fZGF0YT0=";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _txtChat.delegate = self;
    _txtMessage.delegate = self;
    
    _txtChat.editable = NO;
    _txtMessage.clearsOnBeginEditing = YES;
    
    [_btnSend addTarget:self action:@selector(btnSendTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    
    _session = [[OTSession alloc] initWithApiKey:kApiKey
                                       sessionId:kSessionId
                                        delegate:self];
    
    OTError *error = nil;
    
    [_session connectWithToken:kToken error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark OTSession delegate callbacks

- (void) session:(OTSession*)session receivedSignalType:(NSString*)type fromConnection:(OTConnection*)connection withString:(NSString*)string
{
    /*
     The type parameter is a string value that clients can filter on when listening for signals. Set this to an empty string if you do not need to set a type.
     */
    
    NSLog(@"receivedSignalType (%@, %@, %@)", type, connection.connectionId, string);
    
    NSString *currentChat = [NSString stringWithFormat:@"%@", _txtChat.text];
    
    _txtChat.textAlignment = NSTextAlignmentRight;
    
    
    _txtChat.text = [NSString stringWithFormat:@"%@ \n %@", currentChat, string];

}

- (void) session:(OTSession*)session didFailWithError:(OTError*)error
{
    NSLog(@"didFailWithError: (%@)", error);
}

- (void)sessionDidDisconnect:(OTSession*)session
{
    NSString* alertMessage = [NSString stringWithFormat:@"Session disconnected: (%@)", session.sessionId];
    NSLog(@"sessionDidDisconnect (%@)", alertMessage);
}

- (void)sessionDidConnect:(OTSession*)session
{
    NSLog(@"sessionDidConnect (%@)", session.sessionId);
    
    _connection = session.connection;

}

- (void)session:(OTSession *)session connectionCreated:(OTConnection *)connection
{
    NSLog(@"connectionCreated (%@)", connection.connectionId);
}

- (void)session:(OTSession *)session connectionDestroyed:(OTConnection *)connection
{
    NSLog(@"connectionDestroyed (%@)", connection.connectionId);
}

- (void)session:(OTSession*)session streamCreated:(OTStream*)stream
{

}

- (void)session:(OTSession*)session streamDestroyed:(OTStream*)stream
{

}

#pragma mark -
#pragma mark Buttons

- (void)btnSendTouchUpInside:(id)sender
{
    // Cast this if needed
    //UIButton *theButton = (UIButton*)sender;
    OTError *error = nil;
    
    NSString *type = [[NSString alloc] initWithFormat:@""];
    
    [_session signalWithType:type string:_txtMessage.text connection:nil error:&error];
    
    if (error) {
        [self showAlert:[error localizedDescription]];
    }
    
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqual:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    [sender becomeFirstResponder];
}

-(void)textFieldDidEndEditing:(UITextField *)sender
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)sender
{
    [sender resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)sender
{
    [sender resignFirstResponder];
    
    return YES;
}

#pragma mark -
#pragma mark Class methods

- (void)showAlert:(NSString *)string
{
    // show alertview on main UI
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OTError"
                                                        message:string
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil] ;
        [alert show];
    });
}

@end
