//
//  AppDelegate.h
//  blabber
//
//  Created by Matthew Teece on 5/28/15.
//  Copyright (c) 2015 Matthew Teece. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

